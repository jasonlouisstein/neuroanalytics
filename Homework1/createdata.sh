#!/bin/bash

for donor in `seq 1 50`
do
    for tp in `seq 1 10`
    do

	rand1=$RANDOM
	rand2=$RANDOM
	rand3=$RANDOM
	rand4=$RANDOM
	rand5=$RANDOM
	
	echo "data" > donor${donor}_tp${tp}.txt;
	echo ${rand1} >> donor${donor}_tp${tp}.txt;
	echo ${rand2} >> donor${donor}_tp${tp}.txt;
	echo ${rand3} >> donor${donor}_tp${tp}.txt;
	echo ${rand4} >> donor${donor}_tp${tp}.txt;
	echo ${rand5} >> donor${donor}_tp${tp}.txt;
	
    done
done
