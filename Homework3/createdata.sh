#!/bin/bash

for donor in `seq 1 100`
do

    ##Get the appropriately zero-padded donor number
    if [ ${donor} -lt 10 ]
    then
	donorfileid="donor00${donor}"
    elif [ ${donor} -lt 100 ]
    then
	donorfileid="donor0${donor}"
    elif [ ${donor} -lt 1000 ]
    then
	donorfileid="donor${donor}"
    fi
    
    
    for tp in `seq 1 10`
    do

	
	##Get the appropriately zero-padded time point
	if [ ${tp} -lt 10 ]
	then
	    tpfileid="tp00${tp}"
	elif [ ${tp} -lt 100 ]
	then
	    tpfileid="tp0${tp}"
	fi
	
	##Set the output filename	  
	outputfname=${donorfileid}_${tpfileid}.txt

	##Output something to the user
	echo "Generating file: ${outputfname}"
	
	##Assign random numbers to be the data within each file
	rand1=$RANDOM
	rand2=$RANDOM
	rand3=$RANDOM
	rand4=$RANDOM
	rand5=$RANDOM
	
	echo "data" > ${outputfname}
	echo ${rand1} >> ${outputfname}
	echo ${rand2} >> ${outputfname}
	echo ${rand3} >> ${outputfname}
	echo ${rand4} >> ${outputfname}
	echo ${rand5} >> ${outputfname}
	
    done
done
